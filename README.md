# Lappis Ping Client

## Linguagem/Dependências

Esse cliente envia uma mensagem ping para um servidor. Foi desenvolvido em ruby
2.2.0. 

Caso você não tenha o ruby instalado na sua máquina, basta utilizar o seguinte
comando (Ubuntu/Debian):

```shell 
  $ sudo apt-get install ruby ruby-dev
```

## Ambiente
- O cliente ping foi desenvolvido utilizando o sistema operacional *Ubuntu 14.04 LTS* 
- O ambiente de desenvolvimento utilizado:
  - Editor: Vim
  - Terminal de Comandos

## Executando o cliente
Execute o seguinte comando:
  
```shell
  $ ruby client_udp.rb <nome da mensagem> <hostname> <port>
```
Exemplo:

```shell
  $ ruby client_udp.rb teste localhost 2081
``` 




