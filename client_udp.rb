require 'socket'
require 'timeout'
# Number of requisitions
request = 10

# Time out is 1 second
timeout = 1

# Usage information
if ARGV.empty? || ARGV.size != 3
  puts "Usage:"
  puts " - $ ruby client_udp.rb [message] [hostname] [port]"
  exit
end

# Input for the ping
msg, host, port = ARGV

# Socket instance
s = UDPSocket.new

# Statistics values
rtt_times = []
lost_packets = 0

# Main loop
request.times do |number|
  s.send(msg, 0, host, port)
  t1=Time.now.to_f
  begin
    timeout(timeout) do
      response, client_address = s.recvfrom(1024)
      t2=Time.now.to_f
      rtt = (t2-t1)*1000
      rtt_times << rtt
      puts response + "->>>> Requisition number: #{number + 1}  Round Trip Time: #{rtt.round(2)}ms"
    end
  rescue Timeout::Error
    puts "Timed out! Lost Packet."
    lost_packets+=1
  end
end

# Statistics
lost_percent = (lost_packets.to_f/request)*100
major_rtt = rtt_times.max
smaller_rtt = rtt_times.min
average_rtt = rtt_times.inject(:+).to_f/rtt_times.size

# Output Information
puts "========= Statistics Ping ========="
puts "#{request} packets transmitted, #{request - lost_packets} received, #{lost_percent}% packet loss"
puts "RTT (max/min/avg): #{major_rtt.round(2)} ms / #{smaller_rtt.round(2)} ms / #{average_rtt.round(2)} ms"
